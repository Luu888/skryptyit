﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Principal;
using System.Management;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Skrypty_by_itDesk
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            //listBox1.Items.Add(Environment.UserName.ToString());
            EnumerateUsers();
        }
        public class NetworkAPI
            {
                // USER_INFO_1 - Strucutre to hold obtained user information  
                [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
                public struct USER_INFO_1
                {
                    public string usri1_name;
                    public string usri1_password;
                    public int usri1_password_age;
                    public int usri1_priv;
                    public string usri1_home_dir;
                    public string comment;
                    public int usri1_flags;
                    public string usri1_script_path;
                }
                // USER_INFO_0 - Structure to hold Just Usernames  
                [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
                public struct USER_INFO_0
                {
                    public String Username;
                }
                // NetUserAdd - To Add Users to a local machine or Network  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserAdd([MarshalAs(UnmanagedType.LPWStr)] string servername, int level, ref USER_INFO_1 buf, int parm_err);
                // NetUserDel - To delete Users from a local machine or Network  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserDel([MarshalAs(UnmanagedType.LPWStr)] string servername, [MarshalAs(UnmanagedType.LPWStr)] string username);
                // NetUserGetInfo - Returns to a struct Information about the specified user  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserGetInfo([MarshalAs(UnmanagedType.LPWStr)] string servername, [MarshalAs(UnmanagedType.LPWStr)] string username, int level, out IntPtr bufptr);
                // NetUserSetInfo - Allows us to modify User information  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserSetInfo([MarshalAs(UnmanagedType.LPWStr)] string servername, [MarshalAs(UnmanagedType.LPWStr)] string username, int level, ref USER_INFO_1 buf, int error);
                // NetUserChangePassword - Allows us to change a users password providing we have it  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserChangePassword([MarshalAs(UnmanagedType.LPWStr)] string domainname, [MarshalAs(UnmanagedType.LPWStr)] string username, [MarshalAs(UnmanagedType.LPWStr)] string oldpassword, [MarshalAs(UnmanagedType.LPWStr)] string newpassword);
                // NetUserEnum - Obtains a list of all users on local machine or network  
                [DllImport("Netapi32.dll")]
                public extern static int NetUserEnum(string servername, int level, int filter, out IntPtr bufptr, int prefmaxlen, out int entriesread, out int totalentries, out int resume_handle);
                // NetAPIBufferFree - Used to clear the Network buffer after NetUserEnum  
                [DllImport("Netapi32.dll")]
                public extern static int NetApiBufferFree(IntPtr Buffer);
                public NetworkAPI()
                {
                    //  
                    // TODO: Add constructor logic here  
                    //  
                }
            }
        public void EnumerateUsers()
        {
            int EntriesRead;
            int TotalEntries;
            int Resume;
            listBox1.Items.Clear();
            IntPtr bufPtr;
            NetworkAPI.NetUserEnum(null, 0, 2, out bufPtr, -1, out EntriesRead, out TotalEntries, out Resume);
            if (EntriesRead > 0)
            {
                NetworkAPI.USER_INFO_0[] Users = new NetworkAPI.USER_INFO_0[EntriesRead];
                IntPtr iter = bufPtr;
                for (int i = 0; i < EntriesRead; i++)
                {
                    Users[i] = (NetworkAPI.USER_INFO_0)Marshal.PtrToStructure(iter, typeof(NetworkAPI.USER_INFO_0));
                    iter = (IntPtr)((int)iter + Marshal.SizeOf(typeof(NetworkAPI.USER_INFO_0)));
                    listBox1.Items.Add(Users[i].Username);
                }
                NetworkAPI.NetApiBufferFree(bufPtr);
            }
            listBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno zablokować wygasanie hasła dla "+ listBox1.SelectedItem.ToString() + "?";
            const string tytul = "Blokada wygasania hasła dla wybranego użytkownika";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                blokujdlawybranego();
                string wiadomoscs = "Zablokowano wygasanie hasła";
                const string tytuls = "Wygasanie hasłą";
                var messs = MessageBox.Show(wiadomoscs, tytuls, MessageBoxButtons.OK);
                this.Close();
            }
            
        }
        public void blokujdlawybranego()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("wmic UserAccount where Name='"+listBox1.SelectedItem.ToString()+"' set PasswordExpires=False");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            button1.PerformClick();
        }
    }
}
