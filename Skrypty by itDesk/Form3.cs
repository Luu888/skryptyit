﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            zmiananazwykomputera();
        }
        public void zmiananazwykomputera()
            {
                String nowanazwa = textBox1.Text;
                String zmienna = "%computername%";
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("set nowanazwa="+nowanazwa);
                cmd.StandardInput.WriteLine("wmic computersystem where name = \""+zmienna+"\" call rename name ="+nowanazwa);
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            }
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
