﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno chcesz użyć oczyszczania?";
            const string tytul = "Oczyszczanie";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                oczyszczanie();
            }

        }
        public void oczyszczanie()
            {
            Process cmd = new Process();
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("%Systemroot%\\system32\\cmd.exe /c cleanmgr /sageset:65535 & cleanmgr /sagerun:65535");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno dodać użytkownika itDesk?";
            const string tytul = "Tworzenie usera itDesk";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                dodajuseraitdesk();
            }
        }
        public void dodajuseraitdesk()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("net user itdesk 123 /add");
                cmd.StandardInput.WriteLine("net user itdesk /passwordchg:no");
                cmd.StandardInput.WriteLine("net localgroup Administratorzy itdesk /add");
                cmd.StandardInput.WriteLine("WMIC USERACCOUNT WHERE \"Name = 'itdesk'\" SET PasswordExpires=FALSE");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            }

        private void button3_Click(object sender, EventArgs e)
        {
            String wiadomosc = "Czy na pewno użyć Ipconfig reset?";
            const string tytul = "Ipconfig Reset";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                ipconfigreset();
            }
            //richTextBox1.
            
        }
        public void ipconfigreset()
            {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("netsh winsock reset");
            //cmd.StandardInput.WriteLine("timeout 10");
            cmd.StandardInput.WriteLine("ipconfig /flushdns");
            //cmd.StandardInput.WriteLine("timeout 10");
            cmd.StandardInput.WriteLine("ipconfig /release");
            //cmd.StandardInput.WriteLine("timeout 10");
            cmd.StandardInput.WriteLine("ipconfig /renew");
            //cmd.StandardInput.WriteLine("pause");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //string s = cmd.StandardOutput.ReadToEnd();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            //richTextBox1.Text = s;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            czyscbufor();
            File.Delete(@"bufor.exe");
        }
        public void czyscbufor()
            {
            //Process cmd = new Process();
            ////cmd.StartInfo.FileName = "cmd.exe";
            //cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            //cmd.StartInfo.RedirectStandardInput = true;
            //cmd.StartInfo.RedirectStandardOutput = true;
            //cmd.StartInfo.CreateNoWindow = true;
            //cmd.StartInfo.UseShellExecute = false;
            //cmd.Start();
            //cmd.StandardInput.WriteLine("@echo off");
            //cmd.StandardInput.WriteLine("net stop spooler");
            //cmd.StandardInput.WriteLine("del /q /f /s \"C:\\Windows\\system32\\spool\\PRINTERS\\*.* \"");
            //cmd.StandardInput.WriteLine("net start spooler");
            //cmd.StandardInput.Flush();
            //cmd.StandardInput.Close();
            //cmd.WaitForExit();
            //Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            ////string s = cmd.StandardOutput.ReadToEnd();
            //Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            ////richTextBox1.Text += s;
            ////richTextBox1.Text += "Pomyslnie wykonano wszystkie czynnosci";
            byte[] exeBytes = Properties.Resources.bufor;
            string exeToRun = @"bufor.exe";
            if (File.Exists(exeToRun))
            {
                Process.Start(exeToRun);
            }
            else
            {
                using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                {
                    exeFile.Write(exeBytes, 0, exeBytes.Length);
                }

                using (Process exeProcess = Process.Start(exeToRun))
                {
                    exeProcess.WaitForExit();
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String wiadomosc = "Czy na pewno chcesz użyć sprawdzania kluczy office?";
            const string tytul = "Sprawdzanie kluczy office";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                sprawdzkluczoffice();
                File.Delete(@"office.exe");
            }
        }
        public void sprawdzkluczoffice()
            {
            //Process cmd = new Process();
            ////cmd.StartInfo.FileName = "cmd.exe";
            //cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            //cmd.StartInfo.RedirectStandardInput = true;
            //cmd.StartInfo.RedirectStandardOutput = true;
            //cmd.StartInfo.CreateNoWindow = true;
            //cmd.StartInfo.UseShellExecute = false;
            //cmd.Start();
            //cmd.StandardInput.WriteLine("C:");
            //cmd.StandardInput.WriteLine("cd \"C:\\Program Files\\Microsoft Office\\Office16\"");
            //cmd.StandardInput.WriteLine("cd \"C:\\Program Files (x86)\\Microsoft Office\\Office16\"");
            //cmd.StandardInput.WriteLine("cd \"C:\\Program Files\\Microsoft Office\\Office15\"");
            //cmd.StandardInput.WriteLine("cd \"C:\\Program Files(x86)\\Microsoft Office\\Office15\"");
            //cmd.StandardInput.WriteLine("cscript OSPP.VBS /dstatus");
            //cmd.StandardInput.Flush();
            //cmd.StandardInput.Close();
            //cmd.WaitForExit();
            ////string s = cmd.StandardOutput.ReadToEnd();
            //Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            ////richTextBox1.Text = s;byte[] exeBytes = Properties.Resources.mailpv;
            byte[] exeBytes = Properties.Resources.office;
            string exeToRun = @"office.exe";
            if (File.Exists(exeToRun))
            {
                Process.Start(exeToRun);
            }
            else
            {
                using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                {
                    exeFile.Write(exeBytes, 0, exeBytes.Length);
                }

                using (Process exeProcess = Process.Start(exeToRun))
                {
                    exeProcess.WaitForExit();
                }
            }
        }
        private void button7_Click(object sender, EventArgs e)
            {
                String wiadomosc = "Czy na pewno użyć DISM + SFC?";
                const string tytul = "DISM + SFC";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                    {
                        dismsfc();
                        File.Delete(@"skryptsfc.exe");
                    }
            }
        public void dismsfc()
            {
                byte[] exeBytes = Properties.Resources.skryptsfc;
                string exeToRun = @"skryptsfc.exe";
                if (File.Exists(exeToRun))
                    {
                        Process.Start(exeToRun);
                    }
                else
                    {
                        using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                        {
                            exeFile.Write(exeBytes, 0, exeBytes.Length);
                        }

                        using (Process exeProcess = Process.Start(exeToRun))
                        {
                            exeProcess.WaitForExit();
                        }
                    }
            }
        private void button9_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz użyć Fix Credssp rdp?";
                const string tytul = "Fix Credssp rdp";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    fixrdp();
                }
             }
        public void fixrdp()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("REG ADD HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System\\CredSSP\\Parameters /v AllowEncryptionOracle /t REG_DWORD /d 2");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }
        private void button11_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz zablokować usługę Windows Update?";
                const string tytul = "Usługa Windows Update";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    blokujwindows10update();
                }
            }
        private void blokujwindows10update()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("reg add HKLM\\Software\\Policies\\Microsoft\\Windows\\AU /v AUOptions /t REG_DWORD /d 2 /f");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }

        private void button12_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz zablokować Windows Store?";
                const string tytul = "Windows Store";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    blokujsklepwindows10();
                }
        }
        public void blokujsklepwindows10()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("reg add HKLM\\Software\\Policies\\Microsoft\\WindowsStore /v AutoDownload /t REG_DWORD /d 2 /f");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }
        private void pictureBox2_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz zrestartować ten komputer?";
                const string tytul = "Restartowanie komputera";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    restart();
                }
            }
        public void restart()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("shutdown /r /f /t 0");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //string s = cmd.StandardOutput.ReadToEnd();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
            //richTextBox1.Text = s;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz wyłączyć ten komputer?";
                const string tytul = "Wyłączanie komputera";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    wylacz();
                }
            }
        public void wylacz()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("shutdown /s /f /t 0");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }

        private void button6_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz zablokować instalacje sterowników z Windows Update?";
                const string tytul = "Instalacja sterowników z Windows Update";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                    {
                        blokujsterownikiw10();
                        File.Delete(@"windows10blockdrivers.exe");
                    }
            }
        public void blokujsterownikiw10()
            {
                byte[] exeBytes = Properties.Resources.windows10blockdrivers;
                string exeToRun = @"windows10blockdrivers.exe";
                if (File.Exists(exeToRun))
                {
                    Process.Start(exeToRun);
                }
                else
                {
                    using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                    {
                        exeFile.Write(exeBytes, 0, exeBytes.Length);
                    }

                    using (Process exeProcess = Process.Start(exeToRun))
                    {
                        exeProcess.WaitForExit();
                    }
                }
            }

        private void button8_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno chcesz użyć Double Tap?";
            const string tytul = "Double Tap";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                doubletap();
            }
        }
        public void doubletap()
            {
                Process regeditProcess = Process.Start("regedit.exe", "/s right_click_doubletap.reg");
                regeditProcess.WaitForExit();
            }

        private void button10_Click(object sender, EventArgs e)
        {
            new Form2().Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            new Form3().Show();
        }

        private void button13_Click_1(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno chcesz użyć zmiany formatu daty?";
            const string tytul = "Zmiana formatu daty";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                zmianaformatudaty();
            }
        }
        public void zmianaformatudaty()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("REG add \"HKU\\.Default\\Control Panel\\International\" /v sShortDate /t REG_SZ /d rrrr-mm-dd /f");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }
        private void pictureBox4_Click(object sender, EventArgs e)
            {
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", @"cd c:\");
                procStartInfo.WorkingDirectory = @"C:\";
                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit();
            }
        private void button14_Click(object sender, EventArgs e)
        {
            new Form4().Show();
        }
        private void button15_Click(object sender, EventArgs e)
        {
            new Form5().Show();
        }
        private void button16_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz uruchomić zmiane tapety na itDesk?";
                const string tytul = "Logo itDesk";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                    {
                        uruchomlogo();
                        File.Delete(@"Logo_itDesk.exe");
                    }
            }
        public static void uruchomlogo()
            {
            byte[] exeBytes = Properties.Resources.Logo_itDesk;
            string exeToRun = @"Logo_itDesk.exe";
            if (File.Exists(exeToRun))
            {
                Process.Start(exeToRun);
            }
            else
            {
                using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                {
                    exeFile.Write(exeBytes, 0, exeBytes.Length);
                }

                using (Process exeProcess = Process.Start(exeToRun))
                {
                    exeProcess.WaitForExit();
                }
            }
        }
        private void button17_Click(object sender, EventArgs e)
        {
            produkey();
            File.Delete(@"produkey.exe");
            File.Delete(@"produkey.cfg");
        }
        public void produkey()
            {
                byte[] exeBytes = Properties.Resources.produkey;
                string exeToRun = @"produkey.exe";
                if (File.Exists(exeToRun))
                {
                    Process.Start(exeToRun);
                }
                else
                {
                    using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                    {
                        exeFile.Write(exeBytes, 0, exeBytes.Length);
                    }

                    using (Process exeProcess = Process.Start(exeToRun))
                    {
                        exeProcess.WaitForExit();
                    }
                }
        }
        public static string getfile()
        {
            Assembly pliczek = Assembly.GetExecutingAssembly();
            const string name = "Skrypty_by_itDesk.info.txt";
            using (Stream stream = pliczek.GetManifestResourceStream(name))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            //richTextBox1.Text = plik;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno chcesz odblokować instalacje sterowników z Windows Update?";
            const string tytul = "Instalacja sterowników z Windows Update";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                odblokujsterownikiw10();
                File.Delete(@"windows10unblockdrivers.exe");
            }
        }
        public void odblokujsterownikiw10()
            {
                byte[] exeBytes = Properties.Resources.windows10unblockdrivers;
                string exeToRun = @"windows10unblockdrivers.exe";
                if (File.Exists(exeToRun))
                {
                    Process.Start(exeToRun);
                }
                else
                {
                    using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                    {
                        exeFile.Write(exeBytes, 0, exeBytes.Length);
                    }

                    using (Process exeProcess = Process.Start(exeToRun))
                    {
                        exeProcess.WaitForExit();
                    }
                }
            }

        private void button19_Click(object sender, EventArgs e)
            {
                string wiadomosc = "Czy na pewno chcesz odblokować Windows Store?";
                const string tytul = "Windows Store";
                var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                if (mess == DialogResult.Yes)
                {
                    odblokujsklepwindows10();
                } 
            }
        public void odblokujsklepwindows10()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("reg delete HKLM\\Software\\Policies\\Microsoft\\WindowsStore /v AutoDownload /f");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }

        private void button20_Click(object sender, EventArgs e)
            {
                {
                    string wiadomosc = "Czy na pewno chcesz zaktualizować informacje OEM?";
                    const string tytul = "Informacje OEM";
                    var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
                    if (mess == DialogResult.Yes)
                    {
                        oeminfo();
                        File.Delete(@"rejestr.exe");
                    }
                }
            }
        public void oeminfo()
            {
            byte[] exeBytes = Properties.Resources.rejestr;
                string exeToRun = @"rejestr.exe";
                if (File.Exists(exeToRun))
                {
                    Process.Start(exeToRun);
                }
                else
                {
                    using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                    {
                        exeFile.Write(exeBytes, 0, exeBytes.Length);
                    }

                    using (Process exeProcess = Process.Start(exeToRun))
                    {
                        exeProcess.WaitForExit();
                    }
                }
            }

        private void button21_Click(object sender, EventArgs e)
        {
            new Form6().Show();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            wywolajmpv();
            File.Delete(@"mailpv.cfg");
            File.Delete(@"mailpv.exe");
        }
        public void wywolajmpv()
            {
                byte[] exeBytes = Properties.Resources.mailpv;
                string exeToRun = @"mailpv.exe";
                if (File.Exists(exeToRun))
                {
                    Process.Start(exeToRun);
                }
                else
                {
                    using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                    {
                        exeFile.Write(exeBytes, 0, exeBytes.Length);
                    }

                    using (Process exeProcess = Process.Start(exeToRun))
                    {
                        exeProcess.WaitForExit();
                    }
                }
            }

        private void button24_Click(object sender, EventArgs e)
        {
            odblokujwindowsupdate();
        }
        public void odblokujwindowsupdate()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("reg delete HKLM\\Software\\Policies\\Microsoft\\Windows\\AU /v AUOptions /t REG_DWORD /d 2 /f");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                //string s = cmd.StandardOutput.ReadToEnd();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                //richTextBox1.Text = s;
            }

        private void button23_Click(object sender, EventArgs e)
        {
            wywolajnk2edit();
            File.Delete(@"nk2edit.cfg");
            File.Delete(@"nk2edit.exe");
        }
        public void wywolajnk2edit()
        {
            byte[] exeBytes = Properties.Resources.nk2edit;
            string exeToRun = @"nk2edit.exe";
            if (File.Exists(exeToRun))
            {
                Process.Start(exeToRun);
            }
            else
            {
                using (FileStream exeFile = new FileStream(exeToRun, FileMode.CreateNew))
                {
                    exeFile.Write(exeBytes, 0, exeBytes.Length);
                }

                using (Process exeProcess = Process.Start(exeToRun))
                {
                    exeProcess.WaitForExit();
                }
            }
        }
    }
}
