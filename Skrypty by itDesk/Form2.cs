﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dodajusera();
        }
        public void dodajusera()
        {
            string login = textBox1.Text;
            string haslo = textBox2.Text;
            string powthaslo = textBox3.Text;
            if (haslo == powthaslo)
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("net user " + login + " " + haslo + " /add");
                if (checkBox1.Checked == true)
                    {
                        cmd.StandardInput.WriteLine("net user " + login + " /passwordchg:no");
                    }
                else
                    {
                        cmd.StandardInput.WriteLine("net user " + login + " /passwordchg:yes");
                    }
                cmd.StandardInput.WriteLine("net localgroup Administratorzy " + login + " /add");
                if (checkBox2.Checked == true)
                    {
                        cmd.StandardInput.WriteLine("WMIC USERACCOUNT WHERE \"Name = '" + login + "'\" SET PasswordExpires=FALSE");
                    }
                else
                    {
                    cmd.StandardInput.WriteLine("WMIC USERACCOUNT WHERE \"Name = '" + login + "'\" SET PasswordExpires=TRUE");
                }
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                Console.WriteLine(cmd.StandardOutput.ReadToEnd());
                Close();
            }
            else
                {
                    string wiadomosc = "Źle powtórzone hasło";
                    const string tytul = "Error";
                    var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.OK);
                    if (mess == DialogResult.OK)
                        {
                            textBox2.Text = "";
                            textBox3.Text = "";
                        }
                }   
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
