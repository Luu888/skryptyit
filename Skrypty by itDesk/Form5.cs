﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            wylaczanieuspienia();
        }
        public void wylaczanieuspienia()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("powercfg /SETACVALUEINDEX SCHEME_CURRENT 7516b95f-f776-4464-8c53-06167f40cc99 3c0bc021-c8a8-4e07-a973-6b14cbcb2b7e 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -standby-timeout-ac 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -standby-timeout-dc 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -monitor-timeout-dc 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -monitor-timeout-ac 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -hibernate-timeout-ac 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -hibernate-timeout-ac 0");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wlaczanieuspienia();
        }
        public void wlaczanieuspienia()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("powercfg /SETACVALUEINDEX SCHEME_CURRENT 7516b95f-f776-4464-8c53-06167f40cc99 3c0bc021-c8a8-4e07-a973-6b14cbcb2b7e 0");
            cmd.StandardInput.WriteLine("powercfg.exe -change -standby-timeout-ac 60");
            cmd.StandardInput.WriteLine("powercfg.exe -change -standby-timeout-dc 15");
            cmd.StandardInput.WriteLine("powercfg.exe -change -monitor-timeout-dc 5");
            cmd.StandardInput.WriteLine("powercfg.exe -change -monitor-timeout-ac 15");
            cmd.StandardInput.WriteLine("powercfg.exe -change -hibernate-timeout-ac 180");
            cmd.StandardInput.WriteLine("powercfg.exe -change -hibernate-timeout-ac 180");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
        }
    }
}
