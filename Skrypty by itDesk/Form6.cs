﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string wiadomosc = "Czy na pewno zablokować wygasanie haseł dla wszystkich użytkowników?";
            const string tytul = "Blokada wygasania hasła dla wszystkich użytkowników";
            var mess = MessageBox.Show(wiadomosc, tytul, MessageBoxButtons.YesNo);
            if (mess == DialogResult.Yes)
            {
                blokujdlawszystkichuserow();
            }
        }
        public void blokujdlawszystkichuserow()
            {
                Process cmd = new Process();
                //cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();
                cmd.StandardInput.WriteLine("wmic UserAccount set PasswordExpires=False");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
            }

        private void button1_Click(object sender, EventArgs e)
        {
            new Form7().Show();
            this.Close();
        }
    }
}
