﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skrypty_by_itDesk
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wylaczwifi();
        }
        public void wylaczwifi()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("netsh interface set interface Wi-Fi disable");
            System.Threading.Thread.Sleep(60000);
            cmd.StandardInput.WriteLine("netsh interface set interface Wi-Fi enable");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            string s = cmd.StandardOutput.ReadToEnd();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }
        public void wylaczethernet()
        {
            Process cmd = new Process();
            //cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("netsh interface set interface Ethernet disable");
            System.Threading.Thread.Sleep(60000);
            cmd.StandardInput.WriteLine("netsh interface set interface Ethernet enable");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            string s = cmd.StandardOutput.ReadToEnd();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            wylaczethernet();
        }
    }
}
